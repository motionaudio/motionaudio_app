package com.example.admin.motionaudio;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PlayerActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private TextView textViewSong;
    private TextView textViewArtist;
    private Button controlButton;
    private Button resetButton;
    private AudioManipulator audioManipulator;
    private SensorManager sensorManager;

    private ImageView albumArtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaPlayer = MediaPlayer.create(this, getIntent().getData());
        mediaPlayer.setLooping(true);
        setContentView(R.layout.activity_player);
        Uri songUri = getIntent().getData();
        mediaPlayer = MediaPlayer.create(this, songUri);
        initUi();
        showTrackInfo();
        audioManipulator = new AudioManipulator(mediaPlayer);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(audioManipulator, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_UI);
        controlButton.callOnClick();
    }

    private void initUi(){
        textViewSong = (TextView) findViewById(R.id.textview_song);
        textViewArtist = (TextView) findViewById(R.id.textview_artist);
        controlButton = (Button) findViewById(R.id.button_control);
        controlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying()){
                    pause();
                }else {
                    play();
                }
            }
        });
        albumArtView = (ImageView) findViewById(R.id.albumArtView);
        resetButton = (Button) findViewById(R.id.button_reset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioManipulator.reset();
            }
        });
    }

    private void showTrackInfo() {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, getIntent().getData());
        textViewSong.setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
        textViewArtist.setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
        textViewSong.setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
        textViewArtist.setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
        byte[] art = retriever.getEmbeddedPicture();
        if(art != null){
            albumArtView.setImageBitmap(BitmapFactory.decodeByteArray(art, 0, art.length));
        } else {
           albumArtView.setImageResource(R.drawable.no_image);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mediaPlayer.release();
        sensorManager.unregisterListener(audioManipulator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pause();
    }

    private void play(){
        mediaPlayer.start();
        controlButton.setBackground(getDrawable(android.R.drawable.ic_media_pause));
    }

    private void pause(){
        mediaPlayer.pause();
        controlButton.setBackground(getDrawable(android.R.drawable.ic_media_play));
    }
}
