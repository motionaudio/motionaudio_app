package com.example.admin.motionaudio;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.support.annotation.NonNull;

/**
 * Created by mjoss on 27.11.16.
 */

public class AudioManipulator implements SensorEventListener{
    private float[] neutralValues = null;
    private float[] offsetValues = new float[3];

    //Array indexes
    private static final int X = 0;
    private static final int Y = 1;
    private static final int Z = 2;

    private MediaPlayer mediaPlayer;

    public AudioManipulator(@NonNull  MediaPlayer mediaPlayer){
        this.mediaPlayer = mediaPlayer;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR){
            if(neutralValues == null){
                neutralValues = event.values.clone();
            }
            for (int i = X; i <= Z; i++){
                offsetValues[i] = event.values[i] - neutralValues[i];
            }

            PlaybackParams params = mediaPlayer.getPlaybackParams();
            if(Math.abs(offsetValues[X]) >= 0.1){
                params.setPitch(offsetValues[X] + 1);
            }
            if(Math.abs(offsetValues[Y]) >= 0.1){
                params.setSpeed(offsetValues[Y] + 1);
            }

            mediaPlayer.setPlaybackParams(params);
        }
    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public void reset(){
        neutralValues = null;
        mediaPlayer.setPlaybackParams(new PlaybackParams().allowDefaults());
    }


}
