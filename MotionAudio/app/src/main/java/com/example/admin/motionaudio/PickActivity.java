package com.example.admin.motionaudio;

import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

public class PickActivity extends AppCompatActivity {

    private boolean sensorExists;
    private static final int PICK_TRACK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorExists = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sensorExists){
            startPicker();
        }else{
            notifyNoSensor();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == PICK_TRACK){
            data.setClass(this, PlayerActivity.class);
            startActivity(data);
        }
    }

    private void startPicker(){
        Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pickIntent.setType("audio/*");
        startActivityForResult(pickIntent, PICK_TRACK);
    }

    private void notifyNoSensor(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.noGyroscopeMessage));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.noGyroscopeQuit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                PickActivity.this.finish();
            }
        });
        alertDialog.show();
    }
}
